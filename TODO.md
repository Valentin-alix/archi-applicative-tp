
# Exercice 1 - 5 points

## Factory - 1pt

Centraliser la création des licornes et de l'environnement dans une nouvelle classe **Fabrique**.
Il ne doit pas y avoir de couplage fort entre les classes **Licorne** et **Environnement**.


## Pattern singleton - 1pt

Il ne doit y avoir qu'une seule instance possible de la classe **Fabrique**.
Ce contrôle doit être garanti quelque soit l'utilisation de la classe **Fabrique**.


## Strategy - 1pt

Utiliser le patron de conception Strategy afin de traiter les différents types de déplacement d'une licorne sur un terrain. Les stratégies de déplacement seront les suivantes :
- voler (50m par seconde) possible s'il n'y a pas de nuage
- galoper (20m par seconde) possible si il y a strictement moins de 10 cailloux
- marcher (5m par seconde) possible quelques soient les conditions

Les stratégies de  ne seront pas spécifiques à la Licorne.


## Usage - 1pt 

Compléter la méthode **ProgrammeExercice1.main** afin de créer une Licorne et de lui faire parcourir successivement les environnements suivants pour une distance de 100m :
 - 5 nuages et 6 cailloux
 - 0 nuages et 14 cailloux
 - 10 nuages et 20 cailloux
 - 0 nuages et 20 cailloux
 - 1 nuages et 20 cailloux

Pour chaque déplacement, le nom de la Licorne, la nature et la vitesse de déplacement, le temps nécessaire pour parcourir 100m devront être affichés sur une seule ligne.


## Conception - 1pt

Créer une image png ou jpeg illustrant par un diagramme de classes les classes de cet exercice. 
(0.25pt pour l'ensemble des classes et 0.25pt par pattern représenté correctement sur ce diagramme).



# Exercice 2 - 5 pts

Un animal est nourri une fois par jour.
Chaque animal est placé dans un box.
Par jour, une Licorne consomme 5 choux, une chèvre 3 choux et un mouton 2 choux.


## Héritage - 1pt

Ajouter à votre élevage de **Licorne**s, des **Mouton**s et des **Chevre**s (sous la forme de classes).
AJouter une classe **Box** qui ne pourra contenir qu'un animal et une quantité de choux. Chaque **Box** possède un numéro qui lui est propre (incrémentation de 1 au nombre de box).

## Builder - 1pt

Stocker dans une **Ferme** le cheptel. La **Ferme** est formellement construite après l'ajout des animaux quelle doit contenir. A la construction, l'ajout d'un animal se traduit par l'ajout d'un **Box** contenant l'animal et aucun chou. 
Il ne doit pas être possible de créer une ferme sans **Box**. Une **RuntimeException** peut être générée si l'on essaie de construire une ferme sans **Box**.


## Visitor - 1pt

Le fermier se déplace de box en box pour nourrir ses animaux.

Utiliser le patron de conception *Visitor* afin de permettre au fermier de nourrir chacun des animaux de la ferme.
La nourriture déposée dans le Box correspond à la ration d'une journée + 2 choux.
Le point d'entrée sera la méthode **nourrir(int nombreChoux)** de la classe **Ferme**.

Utiliser ce même pattern *Visitor* pour que les animaux puissent se nourrir pour la journée à partir de la nature dans leur **Box**. Si la quantité de choux disponibles dans le **Box** n'est pas suffisante, l'animal se contente de manger les choux disponibles.


## Observer - 1pt

Certains animaux crient famine !

Les animaux ne mangent pas à leur faim : certains sautent un repas.

Pour y remédier, implémenter le patron de conception *Observer* afin que chaque animal puisse notifier au moment du repas qu'il lui manque N choux pour terminer son repas. Le fermier (observateur) bondira pour amener au **Box** concerné le nombre de choux manquant + la ration du lendemain. 



## Vérification - 1pt

Compléter la méthode **ProgrammeExercice2.main** afin de créer une **Ferme** avec 5 **Licorne**s, 3 **Mouton**s  et 10 **Chevre**s.

La quantite de nourriture disponible est illimitée pour le fermier.

Le fermier effectue sa toute première tournée et attend que les animaux réclament le repas au fil des 4 jours suivants.

Sur la console, le fermier doit indiquer le numéro du box, le type d'animal et la quantité de nourriture déposée.


