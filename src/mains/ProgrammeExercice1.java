package mains;
import factorys.FabriqueLicorn;

public class ProgrammeExercice1 {

	public static void main(String[] args) {
		FabriqueLicorn instance = FabriqueLicorn.getOrCreate("aphollon", "red");

		instance.environment.setNombreNuages(5);
		instance.environment.setNombreCailloux(6);
		instance.licorne.galoper(instance.environment);

		instance.environment.setNombreNuages(0);
		instance.environment.setNombreCailloux(14);
		instance.licorne.voler(instance.environment);

		instance.environment.setNombreNuages(10);
		instance.environment.setNombreCailloux(20);
		instance.licorne.marcher(instance.environment);

		instance.environment.setNombreNuages(0);
		instance.environment.setNombreCailloux(20);
		instance.licorne.marcher(instance.environment);

		instance.environment.setNombreNuages(1);
		instance.environment.setNombreCailloux(20);
		instance.licorne.marcher(instance.environment);
	}

}
