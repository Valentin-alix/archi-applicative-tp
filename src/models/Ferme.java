package models;

import java.util.ArrayList;

public class Ferme {
	private ArrayList<Box> boxs;

	public Ferme(ArrayList<Box> boxs) {
		if (boxs.size() == 0) {
			throw new RuntimeException();
		} else {
			this.boxs = boxs;
		}
	}

	public ArrayList<Box> getBoxs() {
		return boxs;
	}

}
