package models;

public interface MoveStrategy {
	void voler(Environnement environment);

	void galoper(Environnement environment);

	void marcher(Environnement environment);

}
