package models;

public class Environnement {

    private int nombreNuages = 0;

    private int nombreCailloux = 0;

    public int getNombreNuages() {
        return nombreNuages;
    }

    public void setNombreNuages(int nombreNuages) {
        this.nombreNuages = nombreNuages;
    }

    public int getNombreCailloux() {
        return nombreCailloux;
    }

    public void setNombreCailloux(int nombreCailloux) {
        this.nombreCailloux = nombreCailloux;
    }
}
