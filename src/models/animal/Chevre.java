package models.animal;

import models.Box;
import models.FeedStrategy;

public class Chevre extends Animal implements FeedStrategy {

	public Chevre() {

	}

	@Override
	public void nourrir(Box box) {
		if (box.getQuantit�Choux() >= 3) {
			box.setQuantit�Choux(box.getQuantit�Choux() - 3);
			System.out.println("Miam ! je suis rassassi� !");
		} else {
			box.setQuantit�Choux(0);
			System.out.println("J'ai toujours faim :(");
		}
	}
}
