package models.animal;

import models.Box;
import models.FeedStrategy;

public class Mouton extends Animal implements FeedStrategy {
	public Mouton() {

	}

	@Override
	public void nourrir(Box box) {
		if (box.getQuantit�Choux() >= 2) {
			box.setQuantit�Choux(box.getQuantit�Choux() - 2);
			System.out.println("Miam ! je suis rassassi� !");
		} else {
			box.setQuantit�Choux(0);
			System.out.println("J'ai toujours faim :(");
		}
	}
}
