package models.animal;

import models.Box;
import models.Environnement;
import models.FeedStrategy;
import models.MoveStrategy;

public class Licorne extends Animal implements MoveStrategy, FeedStrategy {

	private String nom;
	private String couleur;

	public Licorne(String nom, String couleur) {
		this.nom = nom;
		this.couleur = couleur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	@Override
	public void voler(Environnement environment) {
		if (environment.getNombreNuages() == 0) {
			System.out.println("Je vole 50 m�tres par seconde wouhou !");
		}
	}

	@Override
	public void galoper(Environnement environment) {
		if (environment.getNombreCailloux() < 10) {
			System.out.println("Je galope 20 m�tres par seconde wouhou !");
		}
	}

	@Override
	public void marcher(Environnement environment) {
		System.out.println("Je marche 5 m�tres par seconde wouh� !");
	}

	@Override
	public void nourrir(Box box) {
		if (box.getQuantit�Choux() >= 5) {
			box.setQuantit�Choux(box.getQuantit�Choux() - 5);
			System.out.println("Miam ! je suis rassassi� !");
		} else {
			box.setQuantit�Choux(0);
			System.out.println("J'ai toujours faim :(");
		}
	}
}
