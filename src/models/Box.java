package models;

import models.animal.Animal;

public class Box {
	private int numero;
	private Animal animal;
	private int quantitéChoux;

	public Box(int numero, Animal animal, int quantitéChoux) {
		this.animal = animal;
		this.numero = numero;
		this.quantitéChoux = quantitéChoux;
	}

	public int getQuantitéChoux() {
		return this.quantitéChoux;
	}

	public void setQuantitéChoux(int quantitéChoux) {
		this.quantitéChoux = quantitéChoux;
	}
}
