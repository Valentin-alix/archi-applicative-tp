package factorys;
import models.Environnement;
import models.animal.Licorne;

public final class FabriqueLicorn {
	private static FabriqueLicorn INSTANCE;
	public Licorne licorne;
	public Environnement environment;

	private FabriqueLicorn(Licorne licorne, Environnement environment) {
		this.licorne = licorne;
		this.environment = environment;
	}

	public static FabriqueLicorn getOrCreate(String name, String color) {
		if (INSTANCE == null) {
			INSTANCE = new FabriqueLicorn(new Licorne(name, color), new Environnement());
		}
		return INSTANCE;
	}

}
